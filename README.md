## Interview Store

This is a sample Rails app to be used for interview pair sessions. It is a starting point for an e-commerce-like platform. It is made up of Users, Products, and ProductInventories. 

This application supports a basic product list JSON API endpoint which allows filtering products by name and returns the current state of available inventory for each product.

Please see the Github issues list for some sample story ideas to use when pairing with a candidate. 
