class ProductSearch
  attr_reader :options
  def initialize(search_options)
    @options = search_options
    @query = Product.all
  end

  def results
    @query = @query.where("name LIKE ?", "%#{options[:query]}%") if options[:query]
    @query.to_a
  end
end
