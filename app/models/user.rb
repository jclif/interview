class User < ActiveRecord::Base
  validates :email, :auth_token, presence: true
end
