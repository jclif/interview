class ProductInventory < ActiveRecord::Base
  VALID_SIZES = ["S", "M", "L", "XL"]
  belongs_to :product
  validates :size, presence: true, inclusion: { in: VALID_SIZES }
end
