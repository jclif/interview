class Product < ActiveRecord::Base
  validates :name, :price, :description, presence: true
  has_many :product_inventories

  def quantity_in_stock
    product_inventories.count
  end

  def inventory_by_size
    ProductInventory.where(product_id: id).group(:size).count.to_a
  end
end
