module V1
  class ProductsController < ApplicationController

    def index
      @products = ProductSearch.new(params[:filter] || {}).results
    end
  end
end
