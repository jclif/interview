require 'spec_helper'

describe ProductSearch do
  before do
    @to_find = FactoryGirl.create(:product, name: "the Cool product")
    FactoryGirl.create(:product, name: "blah blah")
  end

  it 'returns all results if no options are passed' do
    ProductSearch.new({}).results.should =~ Product.all.to_a
  end

  context 'it allows searching by name' do
    it 'and returns only the results which match the query string' do
      ProductSearch.new(query: "").results.should =~ Product.all.to_a
      ProductSearch.new(query: "cool").results.should == [@to_find]
    end
  end
end
