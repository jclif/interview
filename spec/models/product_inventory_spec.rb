require 'spec_helper' 

describe ProductInventory do
  it { should belong_to(:product) }
  it { should validate_presence_of(:size) }
  it { should allow_value("S").for(:size) }
  it { should_not allow_value("blah").for(:size) }
end
