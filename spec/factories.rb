FactoryGirl.define do 
  factory :user do
    email { Faker::Internet.email }
    auth_token { SecureRandom.uuid }
  end

  factory :product do
    name { Faker::Lorem.characters(10) }
    description { Faker::Lorem.characters(30) }
    price { 3500 }
  end

  factory :product_inventory do
    association :product
    size { "M" }
  end
end
