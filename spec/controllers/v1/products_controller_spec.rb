require 'spec_helper'

describe V1::ProductsController do
  render_views
  before do
    @product = FactoryGirl.create(:product, name: "petes product")
    FactoryGirl.create(:product, name: "Joes product")
  end

  it 'will return a list of all products' do
    get :index, format: :json
    response.should be_successful
    json = JSON.parse(response.body)
    json["products"].length.should == 2
    json["products"].first["name"].should == @product.name
    json["products"].first["id"].should == @product.id
    json["products"].first["description"].should == @product.description
    json["products"].first["price"].should == @product.price
    json["products"].first["quantity_in_stock"].should == 0
    json["products"].first["available_variants"].should == []
  end

  it 'will allow searching via the filter parameters and it will only return the results that match' do
    get :index, filter: { query: "pete" }, format: :json
    response.should be_successful
    json = JSON.parse(response.body)
    json["products"].length.should == 1
    json["products"].first["name"].should == @product.name
  end


  context 'with a size in stock' do
    before do
      FactoryGirl.create(:product_inventory, product: @product, size: "S")
    end

    it 'will return the appropriate inventory information' do
      get :index, format: :json
      response.should be_successful
      json = JSON.parse(response.body)
      json["products"].first["quantity_in_stock"].should == 1
      json["products"].first["available_variants"].should == [{"size" => "S", "quantity" => 1}]
    end 
  end
end
