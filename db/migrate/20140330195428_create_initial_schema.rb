class CreateInitialSchema < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.integer :price
      t.timestamps
    end

    create_table :product_inventories do |t|
      t.string :size
      t.integer :product_id
      t.timestamps
    end

    create_table :users do |t|
      t.string :email
      t.string :auth_token
      t.timestamps
    end
  end
end
