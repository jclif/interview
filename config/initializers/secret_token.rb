# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
InterviewStore::Application.config.secret_key_base = 'a3ea1614fe4e48e5054a5e94cd509ec999d4a36568898f4af735568feb9d512047fb9eb229df0caaf653fa028710527bd6ab4315b9575b791306f1b7694932dd'
